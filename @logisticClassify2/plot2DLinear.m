function plot2DLinear(obj, X, Y)
% plot2DLinear(obj, X,Y)
%   plot a linear classifier (data and decision boundary) when features X are 2-dim
%   wts are 1x3,  wts(1)+wts(2)*X(1)+wts(3)*X(2)
%

    [n,d] = size(X);
    if (d~=2) 
        error('Sorry -- plot2DLogistic only works on 2D data...');
    end
    

    %scatter of the the classes and line defined by weights
    %only works if exactly one of the class values is 1
    scatter(X(Y~=1, 1), X(Y~=1, 2), 30, [0 0 1]);      
    hold on
    scatter(X(Y==1, 1), X(Y==1, 2), 30, [1 0 0]);  
    xline = [min(X(:,1))-1:.01:max(X(:,1))+1];
    yline = -(obj.wts(1)+obj.wts(2).*xline)./obj.wts(3);
    plot(xline, yline,'k')
    ylim([min(X(:,2))-1 max(X(:,2)+1)])
    hold off;

    
end

