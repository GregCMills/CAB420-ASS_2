function Yte = predict(obj,Xte)
% Yhat = predict(obj, X)  : make predictions on test data X

% (1) make predictions based on the sign of wts(1) + wts(2)*x(:,1) + ...
% (2) convert predictions to saved classes: Yte = obj.classes( [1 or 2] );

yte_temp = sign(obj.wts(1) + obj.wts(2) * Xte(:,1) + obj.wts(3) * Xte(:,2));

Yte = zeros(length(Xte), 1);

Yte(yte_temp == -1) = obj.classes(1);
Yte(yte_temp == 1) = obj.classes(2);