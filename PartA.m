clear; close all; clc;

load SVM/data_ps3_2.mat
% C = 1000;
% 
% % Q(1) ********************************************************************
% svm_test(@Klinear, 1, C, set1_train, set1_test)
% title('Set 1 - Linear SVM');
% xlabel('X');
% ylabel('Y');
% 
% svm_test(@Kpoly, 2, C, set2_train, set2_test)
% title('Set 2 - 2nd order polynomial SVM');
% xlabel('X');
% ylabel('Y');
% 
% svm_test(@Kgaussian, 1, C, set3_train, set3_test)
% title('Set 3 - gaussian with stddev = 1 SVM');
% xlabel('X');
% ylabel('Y');
% % *************************************************************************
% 
% % Q(2) ********************************************************************
% % Linear - SVM
% % Train against set4_train
% svm = svm_train(set4_train, @Klinear, 1 , C);
% 
% % verify for training data
% y_est = sign(svm_discrim_func(set4_train.X,svm));
% errors = find(y_est ~= set4_train.y);
% if (errors)
%     fprintf('WARNING: %d training examples were misclassified!!!\n',length(errors));
% end
% % evaluate against test data set4_test
% y_est = sign(svm_discrim_func(set4_test.X,svm));
% errors = find(y_est ~= set4_test.y);
% fprintf('TEST RESULTS: %g of test examples were misclassified.\n',...
%     length(errors)/length(set4_test.y));
% 
% 
% % 2nd Degree Polynomial
% % Train against set4_train
% svm = svm_train(set4_train, @Kpoly, 2 , C);
% % verify for training data
% y_est = sign(svm_discrim_func(set4_train.X,svm));
% errors = find(y_est ~= set4_train.y);
% if (errors)
%     fprintf('WARNING: %d training examples were misclassified!!!\n',length(errors));
% end
% % evaluate against test data
% y_est = sign(svm_discrim_func(set4_test.X,svm));
% errors = find(y_est ~= set4_test.y);
% fprintf('TEST RESULTS: %g of test examples were misclassified.\n',...
%     length(errors)/length(set4_test.y));
% 
% % 2nd Degree Gaussian of standard deviation 1.5
% % Train against set4_train
% svm = svm_train(set4_train, @Kgaussian, 1.5 , C);
% % verify for training data
% y_est = sign(svm_discrim_func(set4_train.X,svm));
% errors = find(y_est ~= set4_train.y);
% if (errors)
%     fprintf('WARNING: %d training examples were misclassified!!!\n',length(errors));
% end
% % evaluate against test data
% y_est = sign(svm_discrim_func(set4_test.X,svm));
% errors = find(y_est ~= set4_test.y);
% fprintf('TEST RESULTS: %g of test examples were misclassified.\n',...
%     length(errors)/length(set4_test.y));

% Logistic Regression Comparison
error = logReg(set4_train.X, set4_train.y, set4_test.X, set4_test.y);
fprintf('Logistic Regression error is %f', error);
% *************************************************************************