clear; clc; close all


X = load('data/faces.txt');
figure;
img = reshape(X(20, :), [24 24]);
imagesc(img); axis square; colormap gray;
% (a)**********************************************************************
%Move the data to center on y axis
mu = mean(X);
X0 = X - mu;
[U, S, V] = svd(X0);
W = U * S;
% *************************************************************************

% (b)**********************************************************************
maxK = 576;
mse = zeros(maxK,1);
for K = 1:maxK
    X0_approx = W(:, 1:K)*V(:,1:K)';
    mse(K) = mean(mean((X0 - X0_approx).^2));
end
figure;
plot(1:maxK, mse);
% *************************************************************************

% (c)**********************************************************************
j = 1;
alpha = 2*median(abs(W(:,j)));

pd1pos = mu + alpha * V(:,j)';
pd1neg = mu - alpha * V(:,j)';

figure;
img = reshape(pd1pos, [24 24]);
imagesc(img); axis square; colormap gray;

figure;
img = reshape(pd1neg, [24 24]);
imagesc(img); axis square; colormap gray;
% *************************************************************************

% (d)**********************************************************************
idx = [1:15]; % pick some data at random or otherwise
figure; hold on; axis ij; colormap(gray);
range = max(W(idx,1:2)) - min(W(idx,1:2)); % find range of coordinates to be plotted
scale = [200 200]./range; % want 24x24 to be visible
for i=idx
    imagesc(W(i,1)*scale(1),W(i,2)*scale(2),...
        reshape(X(i,:),24,24)); 
end
% *************************************************************************

% (e)**********************************************************************


kvalues = [5 10 50];
imgs = [20 21];
for img = imgs
    for k = kvalues
        practice = W(img,1) * V(:,1) ;
        for i = 2:k
            practice = practice + W(img,i) * V(:,i);
        end
        practice = practice + mu';
        figure;
        recon = reshape(practice, [24 24]);
        imagesc(recon); axis square; colormap gray;
    end
end

% *************************************************************************
