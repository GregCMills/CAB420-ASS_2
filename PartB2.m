clear; close all; clc;

% (a)**********************************************************************
data = load('data/iris.txt');
X = data(:,[1:2]);
heading = 'Data Scatter';
figure('name', heading);
scatter(X(:,1), X(:,2));
title(heading);
ylabel('x2');
xlabel('x1');
legend('Data Points');
% *************************************************************************

% (b)**********************************************************************
% Run kmeans 20 times on the data, trying the three different
% initialisation methods @ k = 5.
k = 5;
iters = 20;
sumds = zeros(iters, 3);
for i = 1:iters
    [~,~,sumd] = kmeans(X, k, 'random');
    sumds(i, 1) = sumd;
    [~,~,sumd] = kmeans(X, k, 'farthest');
    sumds(i, 2) = sumd;
    [~,~,sumd] = kmeans(X, k, 'k++');
    sumds(i, 3) = sumd;
end
% Print the average scores for each method.
avgSumds = mean(sumds);
fprintf('Average sumd for random init = %f \nAverage sumd for farthestinit = %f \nAverage sumd for k++ init = %f \n',...
    avgSumds(1), avgSumds(2), avgSumds(3));

% Calculating the lowest sumd average over the 3 init options for k= 20
k = 20;
iters = 20;
sumds20 = zeros(iters, 3);
for i = 1:iters
    [~,~,sumd] = kmeans(X, k, 'random');
    sumds20(i, 1) = sumd;
    [~,~,sumd] = kmeans(X, k, 'farthest');
    sumds20(i, 2) = sumd;
    [~,~,sumd] = kmeans(X, k, 'k++');
    sumds20(i, 3) = sumd;
end
% Print the average scores for each method.
avgSumds = mean(sumds20);
fprintf('Average sumd for random init = %f \n Average sumd for farthest init = %f \n Average sumd for k++ init = %f',...
    avgSumds(1), avgSumds(2), avgSumds(3));

% kmeans using farthest initialisation @ k = 5
k = 5;
heading = 'kmeans using farthest initialisation @ k = 5';
figure('name', heading);
[z,c,sumd] = kmeans(X, k, 'farthest');
plotClassify2D([], X, z);
title(heading);
ylabel('x2');
xlabel('x1');
hold on;
centroidPlot(k, X, z);

% kmeans using k++ initialisation @ k = 20
k = 20;
heading = 'kmeans using k++ initialisation @ k = 20';
figure('name', heading);
[z,c,sumd] = kmeans(X, k, 'k++');
plotClassify2D([], X, z);
title(heading);
ylabel('x2');
xlabel('x1');
hold on;
centroidPlot(k, X, z);
% *************************************************************************

% (c)**********************************************************************
%agglom clusters for k=5 with single linkage
k = 5;
heading = 'agglom clusters using single linkage @ k = 5';
figure('name',heading);
[z, join] = agglomCluster(X,k,'min');
plotClassify2D([], X, z);
title(heading);
ylabel('x2');
xlabel('x1');
hold on;
centroidPlot(k, X, z);

%agglom clusters for k=5 with complete linkage
heading = 'agglom clusters using complete linkage @ k = 5';
figure('name',heading);
[z, join] = agglomCluster(X,k,'max');
plotClassify2D([], X, z);
title(heading);
ylabel('x2');
xlabel('x1');
hold on;
centroidPlot(k, X, z);

%agglom clusters for k=20 with single linkage
k = 20;
heading = 'agglom clusters using single linkage @ k = 20';
figure('name',heading);
[z join] = agglomCluster(X,k,'min');
plotClassify2D([], X, z);
title(heading);
ylabel('x2');
xlabel('x1');
hold on;
centroidPlot(k, X, z);

%agglom cluster for k=20 with complete linkage
heading = 'agglom clusters using complete linkage @ k = 20';
figure('name',heading);
[z join] = agglomCluster(X,k,'max');
plotClassify2D([], X, z);
title(heading);
ylabel('x2');
xlabel('x1');
hold on;
centroidPlot(k, X, z);
% *************************************************************************

% (d)**********************************************************************
%emcluster with k=5
figure;
k = 5;
[z,T,soft,ll] = emCluster(X,k,8,'random');


%emcluster with k=20
figure;
k = 20;
[z,T,soft,ll] = emCluster(X,k,10,'random');
% *************************************************************************