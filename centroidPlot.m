function centroidPlot( k, X, z )
%CENTROIDPLOT This function creats a scatter plot of all the centroid
% points for k clusters.
%   k: Number of clusters
%   X: data values (should only have two columns)
%   z: Cluster target values for each X row.

% For each of the k clusters, get the average x1 and x2 value.
centroid = zeros(k,2);
for thisK = 1:k
    centroid(thisK,1) = mean(X((z==thisK),1));
    centroid(thisK,2) = mean(X((z==thisK),2));
end
% Create Scatter Plot for each x1 and x2 value.
scatter(centroid(:,1), centroid(:,2), 100, 'k', 'x', 'linewidth', 2);

end

