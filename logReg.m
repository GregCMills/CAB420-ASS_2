function [ error ] = logReg( X, Y, xTest, yTest )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here


%scatter of A data

%b
%creation of learner object with A data
learner = logisticClassify2();
learner=setClasses(learner, unique(Y));
weights = zeros(1,65);
learner=setWeights(learner, weights);
learner = train(learner, X, Y);


%c
%calculation of error according to decision boundary
yte = predict(learner,xTest);
error = sum(yte ~= yTest) / length(yTest);

end

